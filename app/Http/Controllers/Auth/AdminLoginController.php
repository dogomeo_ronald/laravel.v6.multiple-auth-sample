<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Auth;

class AdminLoginController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest:admin')->except('logout');
    }

    public function login(Request $request)
    {
        if ($request->method() == 'POST')
        {
            //validate the form data
            $this->validate($request, [
                'username' => 'required|min:1',
                'password' => 'required',
            ]);

            //Attemp to log user in
            if (Auth::guard('admin')->attempt(['username'=>$request->username, 'password'=>$request->password], $request->remember)) 
            {
                //if successful, then redirect to the intended location
                return redirect()->intended(route('admin.home'));
            }
            
            //if unsuccessful, then back to login form with input
            return redirect()->back()->withInput($request->only('username', 'remember'))
            ->withErrors(['username'=>'Username or Password incorrect!']);
        }

        return view('auth.admin.admin_login');
    }

    public function logout()
    {
        Auth::guard('admin')->logout();
        return redirect()->route('admin.login');
    }

}

