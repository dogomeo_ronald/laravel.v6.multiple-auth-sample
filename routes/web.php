<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

/** admin */
//Route::match(['get', 'post'], '/super/register', 'AdminController@super_register')->name('super.register');

Route::prefix('admin')->group(function() {
    Route::match(['get', 'post'], '/register', 'Auth\AdminRegisterController@register')->name('admin.register');
    Route::match(['get', 'post'], '/login', 'Auth\AdminLoginController@login')->name('admin.login');
    Route::post('/logout', 'Auth\AdminLoginController@logout')->name('admin.logout');
    Route::get('/home', 'AdminController@home')->name('admin.home');
});
