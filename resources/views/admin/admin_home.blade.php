@extends('layouts.admin_app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Admin page') }}</div>

                <div class="card-body">

                    Admin Home page
                    This sample only

                    {!! Form::open(['url' => route('home'), 'method' => 'GET']) !!}
                        {!! Form::text('email', 'example@gmail.com', ['class'=>'form-control', 'data-id' => '123']) !!}
                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
